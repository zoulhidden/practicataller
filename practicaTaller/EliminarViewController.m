//
//  EliminarViewController.m
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "EliminarViewController.h"
#import "Jugador.h"

@interface EliminarViewController (){
    Jugador * eliminarJugador;
    Jugador * buscarJugador;
}

@end

@implementation EliminarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = _usuario;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBuscar:(id)sender {
    buscarJugador = [[Jugador alloc]init];
    buscarJugador.codigo = _txtCodigo.text;
    buscarJugador.equipo = _equipo;
    [buscarJugador searchJugadorInDataBasebyCodigo];
    
    _lblEdad.text = buscarJugador.edad;
    _lblEquipo.text = buscarJugador.equipo;
    _lblFoto.text = buscarJugador.foto;
    _lblNombre.text = buscarJugador.nombre;
    _lblPieDominante.text = buscarJugador.pieDominante;
    _lblPosicion.text = buscarJugador.posicion;
}
- (IBAction)btnEliminar:(id)sender {
    eliminarJugador = [[Jugador alloc]init];
    eliminarJugador.codigo = _txtCodigo.text;
    
    if([_txtCodigo hasText]){
        if(![eliminarJugador deleteJugadorFromDatabase]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Ha ocurrido un problema o no existe un Jugador con ese codigo" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Exito" message:@"Has eliminado un jugador correctamente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Tienes que ingresar un codigo" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    _txtCodigo.text = @"";
}
@end
