//
//  EquiposViewController.h
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EquiposViewController : UIViewController

@property (strong, nonatomic) NSString *usuario;
@property (strong, nonatomic) NSString *isAdmin;
- (IBAction)btnNacional:(id)sender;
- (IBAction)btnMedellin:(id)sender;
- (IBAction)btnTolima:(id)sender;
- (IBAction)btnMillonarios:(id)sender;
- (IBAction)btnSalir:(id)sender;

@end
