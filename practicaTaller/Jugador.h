//
//  Jugador.h
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface Jugador : NSObject{
    sqlite3 *jugadordb;
}

@property NSString * codigo;
@property NSString * nombre;
@property NSString * posicion;
@property NSString * pieDominante;
@property NSString * edad;
@property NSString * equipo;
@property NSString * foto;

-(void)createDatabaseInDocuments;
-(BOOL)searchJugadorInDataBasebyCodigo;
-(NSMutableArray *)searchAllJugadoresInDataBaseByEquipo:(NSString *)equipo;
-(BOOL)createJugadorInDataBase;
-(BOOL)deleteJugadorFromDatabase;
-(BOOL)updateJugadorInDatabase;

@end
