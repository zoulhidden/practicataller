//
//  RegistroViewController.m
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "RegistroViewController.h"
#import "Logger.h"
#import "EquiposViewController.h"

@interface RegistroViewController ()

@end

@implementation RegistroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)swcAdministrador:(id)sender {
    if([_swcAdmin isOn]){
        _txtClaveAdmin.hidden = NO;
    }else if(![_swcAdmin isOn]){
        [_txtClaveAdmin setHidden:YES];
    }
}
- (IBAction)btnCancelar:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    Logger *logger = [[Logger alloc]init];
    logger.usuario = _txtUsuario.text;
    logger.clave = _txtClave.text;
    [logger searchLoggerInDataBaseByUser];
    
    UINavigationController *navVC = [segue destinationViewController];
    
    ((EquiposViewController *)[navVC viewControllers].firstObject).usuario  = _txtUsuario.text;
    ((EquiposViewController *)[navVC viewControllers].firstObject).isAdmin  = logger.isAdmin;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    NSString *isAdmin = @"No";
    Logger *logger = [[Logger alloc]init];
    logger.usuario = _txtUsuario.text;
    logger.clave = _txtClave.text;
    
    if([_swcAdmin isOn]){
        if([_txtClaveAdmin.text isEqualToString:@"sistemasTaller123"]){
            isAdmin = @"Si";
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Has ingresado la clave para administradores incorrecta" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            return NO;
        }
    }
    if([_txtUsuario hasText] && [_txtClave hasText]){
        if (![logger createLoggerInDataBase:isAdmin]) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Ha ocurrido un problema o ya existe un Logger con ese usuario" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            return NO;
        }else{
            return YES;
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Tienes que ingresar usuario y clave" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return NO;
    }
    
    
}
@end
