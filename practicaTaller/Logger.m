//
//  Logger.m
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "Logger.h"

@interface Logger()

@property(nonatomic, strong) NSString * databasePath;

@end

@implementation Logger

-(void)searchPathDatabase{
    NSString * rutaDoc= [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    _databasePath = [[NSString alloc]initWithString:[rutaDoc stringByAppendingPathComponent:@"logger.db"]];
}

-(void)createDataBaseInDocuments{
    [self searchPathDatabase];
    
    NSFileManager *fm= [[NSFileManager alloc] init];
    //buscar la ruta y guardarla
    NSLog(@"%@", _databasePath);
    
    //buscar el archivo, lo demas es c puro
    
    if ([fm fileExistsAtPath:_databasePath]==NO) {
        NSLog(@"El archivo no existe");
        //C puro se convierte en UTF8string como si fuera un array de caracteres
        const char * dbpath=[_databasePath UTF8String];
        //abrir mi base de datos, con parametros , ruta base de datos y la direccion de memoria de la variable sqlite3, y ocn esto retorna si el archivo no esta lo crea
        if (sqlite3_open(dbpath, &loggerdb)==SQLITE_OK) {
            NSLog(@"El archivo fue creado");
            //va manejar el error
            char * errMsg;
            //my query, creando la tabla sino existe
            const char * sql_stmt= "CREATE TABLE IF NOT EXISTS LOGGER(USUARIO TEXT NOT NULL PRIMARY KEY, CLAVE TEXT NOT NULL, ISADMIN TEXT NOT NULL)";
            //envio mi query sqlite3, SQLITE_OK  es una constante
            if (sqlite3_exec(loggerdb, sql_stmt, NULL, NULL, &errMsg)==  SQLITE_OK) {
                NSLog(@"Tabla Creada Exitosamente!!..");
            }else{
                NSLog(@"Error en crear Tabla!!..:%s", errMsg);
            }
        }else{
            NSLog(@"Error en crear la base de datos");
        }
    }else{
        NSLog(@"El archivo ya existe, no se remplazo");
    }
}
-(BOOL)createLoggerInDataBase:(NSString *)isAdmin{
    [self searchPathDatabase];
    sqlite3_stmt * query;
    const char * db= [_databasePath UTF8String];
    //& direccion de memoria
    if (sqlite3_open(db, &loggerdb)==SQLITE_OK) {
        NSString * insert;
        if([isAdmin isEqualToString:@"Si"]){
            insert = [NSString stringWithFormat:@"INSERT INTO LOGGER (USUARIO, CLAVE, ISADMIN) VALUES(\"%@\",\"%@\",\"%@\")",_usuario,_clave, @"Si"];
        }else{
            insert = [NSString stringWithFormat:@"INSERT INTO LOGGER (USUARIO, CLAVE, ISADMIN) VALUES(\"%@\",\"%@\",\"%@\")",_usuario,_clave, @"No"];
        }
        
        
        const char * insert_sql = [insert UTF8String];
        sqlite3_prepare_v2(loggerdb, insert_sql, -1, &query, NULL);
        if (sqlite3_step(query)==SQLITE_DONE) {
            return YES;
        }
        sqlite3_finalize(query);
        sqlite3_close(loggerdb);
    }
    return NO;
}

-(BOOL)searchLoggerInDataBaseByUser{
    [self searchPathDatabase];
    const char*db = [_databasePath UTF8String];
    //hacer el query
    sqlite3_stmt * query;
    if (sqlite3_open(db, &loggerdb)==SQLITE_OK)
    {
        NSString * select = [NSString stringWithFormat:@"SELECT * FROM LOGGER WHERE USUARIO = \"%@\" AND CLAVE = \"%@\"", _usuario, _clave];
        
        //convertir
        const char * select_sql = [select UTF8String];
        //-1 lo que mas oueda retornar
        if (sqlite3_prepare_v2(loggerdb, select_sql, -1, &query, NULL)==SQLITE_OK) {
            
            //recorre un solo empleado por ello se utiliza un if
            if (sqlite3_step(query)==SQLITE_ROW) {
                //con @"%s" para pasar de c a objetive c
                _usuario= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 0)];
                _clave=[NSString stringWithFormat:@"%s", sqlite3_column_text(query, 1)];
                _isAdmin= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 2)];
            }
            else{
                return NO;
            }
            sqlite3_finalize(query);
        }else{
            return NO;
        }
        //finalizo el query y cierro la base de datos
        sqlite3_close(loggerdb);
    }
    return YES;
}

@end
