//
//  JugadoresViewController.m
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "JugadoresViewController.h"
#import "Logger.h"
#import "DetalleJugadorViewController.h"
#import "InsertarViewController.h"
#import "EditarViewController.h"
#import "EliminarViewController.h"

static NSString *cellidentifier = @"cell";

@interface JugadoresViewController ()

@end

@implementation JugadoresViewController

@synthesize jugadores;
@synthesize jugador;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    jugador = [[Jugador alloc]init];
    jugadores = [[NSMutableArray alloc]init];
    jugadores = [jugador searchAllJugadoresInDataBaseByEquipo:_equipo];
    self.title = _usuario;
    if([_isAdmin isEqualToString:@"Si"]){
        [_btnInsertar setHidden:NO];
        [_btnEditar setHidden:NO];
        [_btnEliminar setHidden:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    UINavigationController *navVC = [segue destinationViewController];
    if([segue.identifier isEqualToString:@"insertar"]){
        //((InsertarViewController *)[navVC viewControllers].firstObject).equipo  = _equipo;
        //((InsertarViewController *)[navVC viewControllers].firstObject).usuario  = _usuario;
        InsertarViewController *insVC = [segue destinationViewController];
        insVC.equipo = _equipo;
        insVC.usuario = _usuario;
    }
    if([segue.identifier isEqualToString:@"editar"]){
        //((EditarViewController *)[navVC viewControllers].firstObject).equipo  = _equipo;
        //((EditarViewController *)[navVC viewControllers].firstObject).usuario  = _usuario;
        EditarViewController *ediVC = [segue destinationViewController];
        ediVC.equipo = _equipo;
        ediVC.usuario = _usuario;
    }
    if([segue.identifier isEqualToString:@"eliminar"]){
        //((EliminarViewController *)[navVC viewControllers].firstObject).equipo  = _equipo;
        //((EliminarViewController *)[navVC viewControllers].firstObject).usuario  = _usuario;
        EliminarViewController *eliVC = [segue destinationViewController];
        eliVC.equipo = _equipo;
        eliVC.usuario = _usuario;
    }
    if([segue.identifier isEqualToString:@"detalle"]){
        //((DetalleJugadorViewController *)[navVC viewControllers].firstObject).equipo  = _equipo;
        //((DetalleJugadorViewController *)[navVC viewControllers].firstObject).usuario  = _usuario;
        DetalleJugadorViewController *detVC = [segue destinationViewController];
        detVC.codigo = sender;
        detVC.usuario = _usuario;
        detVC.equipo = _equipo;
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    return YES;
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    jugadores = [jugador searchAllJugadoresInDataBaseByEquipo:_equipo];
    NSInteger jugs = [jugadores count];
    return jugs;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [menuTableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    Jugador * j = [jugadores objectAtIndex:[indexPath row]];
    cell.textLabel.text = j.nombre;
    cell.detailTextLabel.text = j.posicion;
    cell.imageView.image  = [UIImage imageNamed:j.foto];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Jugador * j = [jugadores objectAtIndex:[indexPath row]];
    
    //jugadorVC.equipo = _equipo;
    //jugadorVC.usuario = _usuario;
    //[self.navigationController pushViewController:jugadorVC animated:YES];
    [self performSegueWithIdentifier:@"detalle" sender:j.codigo];
}
@end
