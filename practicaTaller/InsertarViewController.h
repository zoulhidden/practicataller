//
//  InsertarViewController.h
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsertarViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *txtFoto;
@property (weak, nonatomic) IBOutlet UITextField *txtEdad;
@property (weak, nonatomic) IBOutlet UITextField *txtPieDominante;
@property (weak, nonatomic) IBOutlet UITextField *txtPosicion;
@property (weak, nonatomic) IBOutlet UITextField *txtNombre;
@property (weak, nonatomic) IBOutlet UITextField *txtCodigo;

@property (strong, nonatomic) NSString *equipo;
@property (strong, nonatomic) NSString *usuario;

- (IBAction)btnInsertar:(id)sender;

@end
