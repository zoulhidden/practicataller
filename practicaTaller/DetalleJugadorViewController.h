//
//  DetalleJugadorViewController.h
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jugador.h"

@interface DetalleJugadorViewController : UIViewController{
    Jugador * datosJugador;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgJugador;
@property (weak, nonatomic) IBOutlet UILabel *lblEquipo;
@property (weak, nonatomic) IBOutlet UILabel *lblEdad;
@property (weak, nonatomic) IBOutlet UILabel *lblPieDominante;
@property (weak, nonatomic) IBOutlet UILabel *lblPosicion;
@property (weak, nonatomic) IBOutlet UILabel *lblNombre;
@property (weak, nonatomic) IBOutlet UILabel *lblCodigo;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (strong, nonatomic) NSString *equipo;
@property (strong, nonatomic) NSString *usuario;
@property (strong, nonatomic) NSString *codigo;

@end
