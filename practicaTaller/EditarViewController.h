//
//  EditarViewController.h
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditarViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *txtCodigo;
@property (weak, nonatomic) IBOutlet UITextField *txtNombre;
@property (weak, nonatomic) IBOutlet UITextField *txtPosicion;
@property (weak, nonatomic) IBOutlet UITextField *txtPieDominante;
@property (weak, nonatomic) IBOutlet UITextField *txtEdad;
@property (weak, nonatomic) IBOutlet UITextField *txtEquipo;
@property (weak, nonatomic) IBOutlet UITextField *txtFoto;

@property (strong, nonatomic) NSString *equipo;
@property (strong, nonatomic) NSString *usuario;

- (IBAction)btnActualizar:(id)sender;
- (IBAction)btnBuscar:(id)sender;

@end
