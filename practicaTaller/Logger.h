//
//  Logger.h
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface Logger : NSObject{
    sqlite3 *loggerdb;
}

@property NSString * usuario;
@property NSString * clave;
@property NSString * isAdmin;

-(void)createDataBaseInDocuments;
-(BOOL)createLoggerInDataBase:(NSString *)claveAdmin;
-(BOOL)searchLoggerInDataBaseByUser;

@end
