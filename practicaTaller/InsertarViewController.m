//
//  InsertarViewController.m
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "InsertarViewController.h"
#import "Jugador.h"

@interface InsertarViewController ()

@end

@implementation InsertarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = _usuario;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnInsertar:(id)sender {
    Jugador * jugador = [[Jugador alloc] init];
    jugador.codigo = _txtCodigo.text;
    jugador.nombre = _txtNombre.text;
    jugador.equipo = _equipo;
    jugador.edad = _txtEdad.text;
    jugador.foto = _txtFoto.text;
    jugador.pieDominante = _txtPieDominante.text;
    jugador.posicion = _txtPosicion.text;
    if([_txtCodigo hasText] && [_txtNombre hasText] && [_txtEdad hasText] && [_txtFoto hasText] && [_txtPieDominante hasText] && [_txtPosicion hasText]){
        if(![jugador createJugadorInDataBase]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Ha ocurrido un problema o ya existe un Jugador con ese codigo" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Exito" message:@"Has creado un jugador correctamente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Tienes que ingresar todos los datos" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}
@end
