//
//  RegistroViewController.h
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistroViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtUsuario;
@property (weak, nonatomic) IBOutlet UITextField *txtClave;
- (IBAction)swcAdministrador:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtClaveAdmin;
- (IBAction)btnCancelar:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *swcAdmin;

@end
