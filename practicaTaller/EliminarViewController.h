//
//  EliminarViewController.h
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EliminarViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *txtCodigo;

- (IBAction)btnBuscar:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblNombre;
@property (weak, nonatomic) IBOutlet UILabel *lblPosicion;
@property (weak, nonatomic) IBOutlet UILabel *lblPieDominante;
@property (weak, nonatomic) IBOutlet UILabel *lblEdad;
@property (weak, nonatomic) IBOutlet UILabel *lblEquipo;
@property (weak, nonatomic) IBOutlet UILabel *lblFoto;

@property (strong, nonatomic) NSString *equipo;
@property (strong, nonatomic) NSString *usuario;

- (IBAction)btnEliminar:(id)sender;

@end
