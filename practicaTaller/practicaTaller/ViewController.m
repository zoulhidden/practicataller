//
//  ViewController.m
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "ViewController.h"
#import "Logger.h"
#import "EquiposViewController.h"
#import "RegistroViewController.h"
#import "Jugador.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    Logger * log = [[Logger alloc] init];
    [log createDataBaseInDocuments];
    Jugador * jug = [[Jugador alloc] init];
    [jug createDatabaseInDocuments];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    Logger *logger = [[Logger alloc]init];
    logger.usuario = _txtUsuario.text;
    logger.clave = _txtClave.text;
    [logger searchLoggerInDataBaseByUser];
    UINavigationController *navVC = [segue destinationViewController];
    if([segue.identifier isEqualToString:@"ingreso"]){
        ((EquiposViewController *)[navVC viewControllers].firstObject).usuario  = _txtUsuario.text;
        ((EquiposViewController *)[navVC viewControllers].firstObject).isAdmin  = logger.isAdmin;
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if([identifier isEqualToString:@"ingreso"]){
        Logger *logger = [[Logger alloc]init];
        logger.usuario = _txtUsuario.text;
        logger.clave = _txtClave.text;
        if (![logger searchLoggerInDataBaseByUser]) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Has ingresado algun dato incorrecto o no estas registrado." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        return [logger searchLoggerInDataBaseByUser];
    }else {
        return YES;
    }
}
@end
