//
//  ViewController.h
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *txtUsuario;
@property (weak, nonatomic) IBOutlet UITextField *txtClave;

@end

