//
//  DetalleJugadorViewController.m
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "DetalleJugadorViewController.h"

@interface DetalleJugadorViewController ()

@end

@implementation DetalleJugadorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = _usuario;
    datosJugador = [[Jugador alloc]init];
    datosJugador.codigo = _codigo;
    datosJugador.equipo = _equipo;
    [datosJugador searchJugadorInDataBasebyCodigo];
    _lblCodigo.text = datosJugador.codigo;
    _lblEdad.text = datosJugador.edad;
    _lblEquipo.text = _equipo;
    _lblNombre.text = datosJugador.nombre;
    _lblPieDominante.text = datosJugador.pieDominante;
    _lblPosicion.text = datosJugador.posicion;
    _imgView.image = [UIImage imageNamed:datosJugador.foto];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
