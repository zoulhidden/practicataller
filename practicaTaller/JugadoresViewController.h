//
//  JugadoresViewController.h
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jugador.h"

@interface JugadoresViewController : UIViewController{
    
    __weak IBOutlet UITableView *menuTableView;
    NSMutableArray * jugadores;
    Jugador * jugador;
}

@property (weak, nonatomic) IBOutlet UIButton *btnInsertar;
@property (weak, nonatomic) IBOutlet UIButton *btnEditar;
@property (weak, nonatomic) IBOutlet UIButton *btnEliminar;


@property (strong, nonatomic) NSString *usuario;
@property (strong, nonatomic) NSString *equipo;
@property (strong, nonatomic) NSString *isAdmin;
@property (nonatomic, strong) NSMutableArray *jugadores;
@property (nonatomic, strong) Jugador * jugador;

@end
