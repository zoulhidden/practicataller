//
//  EquiposViewController.m
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "EquiposViewController.h"
#import "JugadoresViewController.h"

@interface EquiposViewController ()

@end

@implementation EquiposViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = _usuario;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    JugadoresViewController *navVC = [segue destinationViewController];
    
    navVC.usuario = _usuario;
    navVC.equipo = segue.identifier;
    navVC.isAdmin = _isAdmin;
}


- (IBAction)btnNacional:(id)sender {
}

- (IBAction)btnMedellin:(id)sender {
}

- (IBAction)btnTolima:(id)sender {
}

- (IBAction)btnMillonarios:(id)sender {
}

- (IBAction)btnSalir:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
