//
//  Jugador.m
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "Jugador.h"

@interface Jugador()

@property(nonatomic, strong) NSString * databasePath;

@end

@implementation Jugador

-(void)searchPathDatabase{
    NSString * rutaDoc= [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    _databasePath = [[NSString alloc]initWithString:[rutaDoc stringByAppendingPathComponent:@"jugador.db"]];
}

-(void)createDatabaseInDocuments{
    [self searchPathDatabase];
    
    NSFileManager *fm= [[NSFileManager alloc] init];
    //buscar la ruta y guardarla
    NSLog(@"%@", _databasePath);
    
    //buscar el archivo, lo demas es c puro
    
    if ([fm fileExistsAtPath:_databasePath]==NO) {
        NSLog(@"El archivo no existe");
        //C puro se convierte en UTF8string como si fuera un array de caracteres
        const char * dbpath=[_databasePath UTF8String];
        //abrir mi base de datos, con parametros , ruta base de datos y la direccion de memoria de la variable sqlite3, y ocn esto retorna si el archivo no esta lo crea
        if (sqlite3_open(dbpath, &jugadordb)==SQLITE_OK) {
            NSLog(@"El archivo fue creado");
            //va manejar el error
            char * errMsg;
            //my query, creando la tabla sino existe
            const char * sql_stmt= "CREATE TABLE IF NOT EXISTS JUGADOR(CODIGO TEXT NOT NULL PRIMARY KEY, NOMBRE TEXT NOT NULL, POSICION TEXT NOT NULL, PIEDOMINANTE TEXT NOT NULL, EDAD TEXT NOT NULL, EQUIPO TEXT NOT NULL, FOTO TEXT NOT NULL)";
            //envio mi query sqlite3, SQLITE_OK  es una constante
            if (sqlite3_exec(jugadordb, sql_stmt, NULL, NULL, &errMsg)==  SQLITE_OK) {
                NSLog(@"Tabla Creada Exitosamente!!..");
            }else{
                NSLog(@"Error en crear Tabla!!..:%s", errMsg);
            }
        }else{
            NSLog(@"Error en crear la base de datos");
        }
    }else{
        NSLog(@"El archivo ya existe, no se remplazo");
    }
}
-(BOOL)searchJugadorInDataBasebyCodigo{
    [self searchPathDatabase];
    const char*db = [_databasePath UTF8String];
    //hacer el query
    sqlite3_stmt * query;
    if (sqlite3_open(db, &jugadordb)==SQLITE_OK)
    {
        NSString * select = [NSString stringWithFormat:@"SELECT * FROM JUGADOR WHERE CODIGO = \"%@\" AND EQUIPO = \"%@\"", _codigo, _equipo];
        
        //convertir
        const char * select_sql = [select UTF8String];
        //-1 lo que mas oueda retornar
        if (sqlite3_prepare_v2(jugadordb, select_sql, -1, &query, NULL)==SQLITE_OK) {
            
            //recorre un solo empleado por ello se utiliza un if
            if (sqlite3_step(query)==SQLITE_ROW) {
                //con @"%s" para pasar de c a objetive c
                _codigo= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 0)];
                _nombre=[NSString stringWithFormat:@"%s", sqlite3_column_text(query, 1)];
                _posicion= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 2)];
                _pieDominante= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 3)];
                _edad= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 4)];
                _equipo= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 5)];
                _foto = [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 6)];
            }else{
                sqlite3_close(jugadordb);
                sqlite3_finalize(query);
                return NO;
            }
            sqlite3_close(jugadordb);
            sqlite3_finalize(query);
        }else{
            sqlite3_close(jugadordb);
            sqlite3_finalize(query);
            return NO;
        }
        //finalizo el query y cierro la base de datos
        sqlite3_close(jugadordb);
    }
    return YES;
}
-(NSMutableArray *)searchAllJugadoresInDataBaseByEquipo:(NSString *)equipo{
    NSMutableArray *listaJugadores = [[NSMutableArray alloc] init];
    [self searchPathDatabase];
    const char*db = [_databasePath UTF8String];
    //hacer el query
    sqlite3_stmt * query;
    if (sqlite3_open(db, &jugadordb)==SQLITE_OK)
    {
        NSString * select = [NSString stringWithFormat:@"SELECT * FROM JUGADOR WHERE EQUIPO = \"%@\"", equipo];
        
        //convertir
        const char * select_sql = [select UTF8String];
        //-1 lo que mas oueda retornar
        if (sqlite3_prepare_v2(jugadordb, select_sql, -1, &query, NULL)==SQLITE_OK) {
            
            //recorre un solo empleado por ello se utiliza un if
            while (sqlite3_step(query)==SQLITE_ROW) {
                //con @"%s" para pasar de c a objetive c
                Jugador * jugador = [[Jugador alloc] init];
                jugador.codigo= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 0)];
                jugador.nombre=[NSString stringWithFormat:@"%s", sqlite3_column_text(query, 1)];
                jugador.posicion= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 2)];
                jugador.pieDominante= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 3)];
                jugador.edad= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 4)];
                jugador.equipo= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 5)];
                jugador.foto = [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 6)];
                
                [listaJugadores addObject:jugador];
            }
            sqlite3_finalize(query);
        }
        //finalizo el query y cierro la base de datos
        sqlite3_close(jugadordb);
    }
    return listaJugadores;
}
-(BOOL)createJugadorInDataBase{
    [self searchPathDatabase];
    sqlite3_stmt * query;
    const char * db= [_databasePath UTF8String];
    //& direccion de memoria
    if (sqlite3_open(db, &jugadordb)==SQLITE_OK) {
        NSString * insert = [NSString stringWithFormat:@"INSERT INTO JUGADOR (CODIGO, NOMBRE, POSICION, PIEDOMINANTE, EDAD, EQUIPO, FOTO) VALUES(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",_codigo,_nombre, _posicion,_pieDominante, _edad, _equipo, _foto];
        
        const char * insert_sql = [insert UTF8String];
        sqlite3_prepare_v2(jugadordb, insert_sql, -1, &query, NULL);
        //NSLog([NSString stringWithFormat:@"%i", e]);
        if (sqlite3_step(query)==SQLITE_DONE) {
            sqlite3_finalize(query);
            sqlite3_close(jugadordb);
            return YES;
        }else{
            //NSLog([NSString stringWithFormat:@"Error al crear el jugador, %i", e]);
        }
        sqlite3_finalize(query);
        sqlite3_close(jugadordb);
    }else{
    NSLog(@"Error al abrir la base de datos");
    }
        return NO;
}
-(BOOL)deleteJugadorFromDatabase{
    [self searchPathDatabase];
    sqlite3_stmt * query;
    const char * db=[_databasePath UTF8String];
    if (sqlite3_open(db, &jugadordb)==SQLITE_OK) {
        NSString * delete =[NSString stringWithFormat:@"DELETE FROM JUGADOR WHERE CODIGO = \"%@\"", _codigo];
        const char * delete_sql =[delete UTF8String];
        sqlite3_prepare_v2(jugadordb, delete_sql, -1, &query, NULL);
        if (sqlite3_step(query)==SQLITE_DONE) {
            sqlite3_finalize(query);
            sqlite3_close(jugadordb);
            return YES;
        }else{
            NSLog(@"Error al borrar el jugador");
        }
        sqlite3_finalize(query);
        sqlite3_close(jugadordb);
    }else{
    NSLog(@"Error al abrir la base de datos");
    }
    return NO;
}
-(BOOL)updateJugadorInDatabase{
    [self searchPathDatabase];
    sqlite3_stmt *query;
    const char * db=[_databasePath UTF8String];
    if (sqlite3_open(db, &jugadordb)==SQLITE_OK) {
        NSString * update =[NSString stringWithFormat:@"UPDATE JUGADOR SET NOMBRE= \"%@\", POSICION= \"%@\", PIEDOMINANTE= \"%@\", EDAD= \"%@\", EQUIPO= \"%@\", FOTO= \"%@\"  WHERE CODIGO = \"%@\"", _nombre,_posicion,_pieDominante, _edad, _equipo, _foto, _codigo];
        const char * update_sql =[update UTF8String];
        sqlite3_prepare_v2(jugadordb, update_sql, -1, &query, NULL);
        if (sqlite3_step(query)==SQLITE_DONE) {
            sqlite3_finalize(query);
            sqlite3_close(jugadordb);
            return YES;
        }else{
            NSLog(@"Error al editar el jugador");
        }
        sqlite3_finalize(query);
        sqlite3_close(jugadordb);
    }else{
    NSLog(@"Error al abrir la base de datos");
    }
    return NO;
}

@end
