//
//  EditarViewController.m
//  practicaTaller
//
//  Created by Mariana Aristizabal Garcia on 27/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "EditarViewController.h"
#import "Jugador.h"
#import "JugadoresViewController.h"

@interface EditarViewController (){
    Jugador * actualizarJugador;
    Jugador * buscarJugador;
}

@end

@implementation EditarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = _usuario;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnActualizar:(id)sender {
    actualizarJugador = [[Jugador alloc]init];
    actualizarJugador.codigo = _txtCodigo.text;
    actualizarJugador.nombre = _txtNombre.text;
    actualizarJugador.equipo = _equipo;
    actualizarJugador.edad = _txtEdad.text;
    actualizarJugador.foto = _txtFoto.text;
    actualizarJugador.pieDominante = _txtPieDominante.text;
    actualizarJugador.posicion = _txtPosicion.text;
    
    if([_txtCodigo hasText] && [_txtNombre hasText] && [_txtEdad hasText] && [_txtFoto hasText] && [_txtPieDominante hasText] && [_txtPosicion hasText]){
        if(![actualizarJugador updateJugadorInDatabase]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Ha ocurrido un problema o no existe un Jugador con ese codigo" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Exito" message:@"Has editado un jugador correctamente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Tienes que ingresar todos los datos" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

- (IBAction)btnBuscar:(id)sender {
    buscarJugador = [[Jugador alloc] init];
    buscarJugador.codigo = _txtCodigo.text;
    buscarJugador.equipo = _equipo;
    [buscarJugador searchJugadorInDataBasebyCodigo];
    
    _txtEdad.text = buscarJugador.edad;
    _txtEquipo.text = buscarJugador.equipo;
    _txtFoto.text = buscarJugador.foto;
    _txtNombre.text = buscarJugador.nombre;
    _txtPieDominante.text = buscarJugador.pieDominante;
    _txtPosicion.text = buscarJugador.posicion;
}
@end
